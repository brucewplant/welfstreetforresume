# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0001_initial'),
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Specials',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Clothing',
        ),
        migrations.AddField(
            model_name='specials',
            name='item',
            field=models.ForeignKey(to='collection.Clothing'),
        ),
    ]
