# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_auto_20150712_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specials',
            name='item',
            field=models.ForeignKey(to='collection.ClothingStyle'),
        ),
    ]
