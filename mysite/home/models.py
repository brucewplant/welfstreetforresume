from django.db import models
from collection.models import ClothingStyle


# This model is depricated and should not be used in
# the current website.
# To appear on the slideshow
class Specials(models.Model):
    item = models.ForeignKey('collection.ClothingStyle')

    def __unicode__(self):
        return self.item.name
