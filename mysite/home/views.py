from django.shortcuts import render
from django.core import serializers
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from .models import Specials
from django.conf import settings
from collection.models import (
    ClothingStyle,
    ClothingImage,
    ClothingItem,
    ClothingPrimaryImage)


def index(request):
    style_objects = ClothingStyle.objects.all().exclude(carousel_image="")
    template = loader.get_template('home/index.html')

    context = RequestContext(request, {
        'style_objects': style_objects,
        'business_email': settings.PAYPAL_RECEIVER_EMAIL,
        })
    return HttpResponse(template.render(context))
