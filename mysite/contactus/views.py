from django.shortcuts import render
from django.core import serializers
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings


def index(request):
    template = loader.get_template('contactus/contactus.html')
    context = RequestContext(request, {
        'business_email': settings.PAYPAL_RECEIVER_EMAIL,
        })
    return HttpResponse(template.render(context))
