from django.shortcuts import render
from django.core import serializers
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from collection.models import (
    ClothingStyle,
    ClothingImage,
    ClothingItem,
    ClothingPrimaryImage)


def index(request):
    print ("TEST")
    if request.method == 'GET':
        product_name = request.GET['product']
        print request
        product = ClothingStyle.objects.filter(name__exact=product_name)
        clothing_colours = product[0].colour.all()
        clothing_cuts = product[0].cut.all()
        principal_image = ClothingPrimaryImage.objects.filter(style=product)
        clothing_images = ClothingImage.objects.filter(style=product)
        sub_styles = ClothingItem.objects.filter(style=product)

        template = loader.get_template('purchase/purchase.html')

        context = RequestContext(request, {
            'product_name': product[0].name,
            'principal_image': principal_image[0].image,
            'clothing_colours': clothing_colours,
            'clothing_cuts': clothing_cuts,
            'product_images': clothing_images,
            'prodcut_description': product[0].description,
            'shipping_description': product[0].shipping_description,
            'product_cost': product[0].cost,
            'business_email': settings.PAYPAL_RECEIVER_EMAIL,
            })

    return HttpResponse(template.render(context))
