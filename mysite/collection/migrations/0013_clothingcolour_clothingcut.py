# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0012_clothingstyle_carousel_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClothingColour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('colour_name', models.CharField(max_length=100)),
                ('clothing_style', models.ManyToManyField(to='collection.ClothingStyle')),
            ],
        ),
        migrations.CreateModel(
            name='ClothingCut',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cut_name', models.CharField(max_length=100)),
                ('clothing_style', models.ManyToManyField(to='collection.ClothingStyle')),
            ],
        ),
    ]
