# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_auto_20151217_1157'),
        ('collection', '0008_auto_20150812_1112'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClothingImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.FileField(upload_to=b'files', blank=True)),
                ('primary', models.BooleanField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ClothingItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('colour', models.CharField(max_length=100)),
                ('cut', models.CharField(max_length=1000)),
                ('stock', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ClothingStyle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=200)),
                ('cost', models.DecimalField(max_digits=6, decimal_places=2)),
                ('description', models.CharField(default=b'', max_length=1000)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.DeleteModel(
            name='Clothing',
        ),
        migrations.AddField(
            model_name='clothingitem',
            name='style',
            field=models.ForeignKey(to='collection.ClothingStyle'),
        ),
        migrations.AddField(
            model_name='clothingimage',
            name='style',
            field=models.ForeignKey(to='collection.ClothingStyle'),
        ),
    ]
