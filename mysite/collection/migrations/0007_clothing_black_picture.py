# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0006_auto_20150713_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='clothing',
            name='black_picture',
            field=models.FileField(upload_to=b'files', blank=True),
        ),
    ]
