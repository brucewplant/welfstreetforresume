# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0013_clothingcolour_clothingcut'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clothingcolour',
            name='clothing_style',
        ),
        migrations.RemoveField(
            model_name='clothingcut',
            name='clothing_style',
        ),
        migrations.AddField(
            model_name='clothingstyle',
            name='colour',
            field=models.ManyToManyField(to='collection.ClothingColour'),
        ),
        migrations.AddField(
            model_name='clothingstyle',
            name='cut',
            field=models.ManyToManyField(to='collection.ClothingCut'),
        ),
    ]
