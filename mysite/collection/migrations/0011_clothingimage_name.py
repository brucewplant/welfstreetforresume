# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0010_auto_20151220_0423'),
    ]

    operations = [
        migrations.AddField(
            model_name='clothingimage',
            name='name',
            field=models.CharField(default='Welf Street Muscle Black', unique=True, max_length=100),
            preserve_default=False,
        ),
    ]
