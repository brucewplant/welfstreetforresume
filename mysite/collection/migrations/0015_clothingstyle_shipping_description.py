# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0014_auto_20151226_0834'),
    ]

    operations = [
        migrations.AddField(
            model_name='clothingstyle',
            name='shipping_description',
            field=models.CharField(default=b'', max_length=1000),
        ),
    ]
