# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0011_clothingimage_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='clothingstyle',
            name='carousel_image',
            field=models.FileField(upload_to=b'files', blank=True),
        ),
    ]
