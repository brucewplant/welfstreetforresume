# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0009_auto_20151217_1157'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClothingPrimaryImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='clothingimage',
            name='primary',
        ),
        migrations.AddField(
            model_name='clothingprimaryimage',
            name='image',
            field=models.OneToOneField(to='collection.ClothingImage'),
        ),
        migrations.AddField(
            model_name='clothingprimaryimage',
            name='style',
            field=models.OneToOneField(to='collection.ClothingStyle'),
        ),
    ]
