# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0003_auto_20150712_1406'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clothing',
            name='picture',
            field=models.FileField(upload_to=b'', blank=True),
        ),
    ]
