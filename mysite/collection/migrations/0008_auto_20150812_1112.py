# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0007_clothing_black_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='clothing',
            name='black_muscle',
            field=models.FileField(upload_to=b'files', blank=True),
        ),
        migrations.AddField(
            model_name='clothing',
            name='white_muscle',
            field=models.FileField(upload_to=b'files', blank=True),
        ),
    ]
