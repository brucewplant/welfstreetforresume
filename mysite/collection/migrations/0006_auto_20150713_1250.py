# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0005_auto_20150713_0731'),
    ]

    operations = [
        migrations.AddField(
            model_name='clothing',
            name='description',
            field=models.CharField(default=b'', max_length=1000),
        ),
        migrations.AlterField(
            model_name='clothing',
            name='name',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]
