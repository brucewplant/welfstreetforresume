from django.db import models


# Create your models here
class ClothingColour(models.Model):
    # Tracks which colours each clothing style can be
    # purchased in. Change to an enumerated type, or
    # programatically enforce HEX codes only.
    colour_name = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % (self.colour_name)


class ClothingCut(models.Model):
    # Tracks which cuts each clothing style can be
    # purchased in.
    cut_name = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % (self.cut_name)


class ClothingStyle(models.Model):
    name = models.CharField(max_length=200, unique=True)
    cost = models.DecimalField(decimal_places=2, max_digits=6)
    # For the moment, product_description will retain the identifier
    # "description" in order to ensure backwards compatibility
    description = models.CharField(max_length=1000, default="")
    shipping_description = models.CharField(max_length=1000, default="")
    carousel_image = models.FileField(upload_to="files", blank=True)
    colour = models.ManyToManyField(ClothingColour)
    cut = models.ManyToManyField(ClothingCut)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class ClothingImage(models.Model):
    name = models.CharField(max_length=100, unique=True)
    image = models.FileField(upload_to="files", blank=True)
    style = models.ForeignKey(ClothingStyle)
    # #  Make sure that only one is set to primary at a time programatically
    # primary = models.BooleanField(default=0)

    def __str__(self):
        return "%s %s" % (self.style.name, self.name)


class ClothingItem(models.Model):
    colour = models.CharField(max_length=100)
    cut = models.CharField(max_length=1000)
    stock = models.IntegerField()
    style = models.ForeignKey(ClothingStyle)
    # Add a field for the PayPal button

    def __str__(self):
        return "%s %s %s" % (self.style.name,
                             self.colour,
                             self.cut)


class ClothingPrimaryImage(models.Model):
    style = models.OneToOneField(ClothingStyle)
    image = models.OneToOneField(ClothingImage)

    def __str__(self):
        return "Primary Image of %s" % (self.style.name)
