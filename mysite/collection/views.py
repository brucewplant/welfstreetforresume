from django.shortcuts import render
from django.core import serializers
from django.template import RequestContext, loader
from django.http import HttpResponse
from django.conf import settings

from .models import ClothingStyle, ClothingPrimaryImage


def index(request):
    clothing_objects = ClothingPrimaryImage.objects.all()
    print "HERE IS THE TYPE:", type(clothing_objects)
    template = loader.get_template('collection/collection.html')
    context = RequestContext(request, {
        'clothing_objects': clothing_objects,
        'business_email': settings.PAYPAL_RECEIVER_EMAIL,
        })
    return HttpResponse(template.render(context))
