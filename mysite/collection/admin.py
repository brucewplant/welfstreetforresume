from django.contrib import admin

from .models import (
    ClothingStyle, ClothingItem,
    ClothingImage, ClothingPrimaryImage, ClothingColour,
    ClothingCut)


class ClothingAdmin(admin.ModelAdmin):
    fields = ['name', 'cost']


admin.site.register(ClothingStyle)
admin.site.register(ClothingImage)
admin.site.register(ClothingPrimaryImage)
admin.site.register(ClothingColour)
admin.site.register(ClothingCut)
